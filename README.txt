
Taxonomy Blacklist Module
-------------------------
by Marco Olivo, me@olivo.net - www.olivo.net



Description
-----------
With this module you can flag several terms as "blacklisted" and forbid them to appear
inside your taxonomies.
This can come in quite handy at least in these cases:

 * you have an external module (such as yahoo_terms) that automatically categories
   your content and you don't want certain words to appear in the generated taxonomies
   (in particular, Yahoo is known to extract some common words in several languages,
   and this module could certainly help you in this case)

 * your users can use taxonomies to tag content and/or other users, etc., and you don't
   want bad words to appear inside your taxonomies


Installation
------------
Simple copy the files in your drupal modules directory and activate the module.


How to use it
-------------
Go to 'admin/content/taxonomy_blacklist' to manually add badwords you don't want
to appear in *any* of your taxonomies. Please note that taxonomy blacklisting can
currently be enabled or disabled on all your taxonomies, and not only one or a few of them.

Using specific module settings you can enable/disable blacklisting and also have the module
use the metaphone value of the words when checking for badwords. By using it, you
can extend the range of variations of badwords that can be covered by looking
also at the sound of the words, instead of matching the exact word. This feature works
well only for the English language, since the PHP API used knows only English pronunciation.
